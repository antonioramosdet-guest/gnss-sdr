Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gnss-sdr
Upstream-Contact: Carles Fernandez <carles.fernandez@cttc.es>
Source: git://github.com/gnss-sdr/gnss-sdr
Comment:
 See the AUTHORS file for a more complete list of contributors.
Copyright:
 (C) 2010-2016 Carles Fernandez <carles.fernandez@cttc.es>
 (C) 2010-2016 Javier Arribas <javier.arribas@cttc.es>
 (C) 2010 Carlos Aviles <carlos.avilesr@googlemail.es>
 (C) 2012-2016 Luis Esteve <luis.esteve.elfau@gmail.es>
License: GPL-3+

Files: *
Copyright:
 (C) 2010-2016 Carles Fernandez <carles.fernandez@cttc.es>
 (C) 2010-2016 Javier Arribas <javier.arribas@cttc.es>
 (C) 2010 Carlos Aviles <carlos.avilesr@googlemail.es>
 (C) 2012-2016 Luis Esteve <luis.esteve.elfau@gmail.es>
License: GPL-3+

Files: src/core/libs/supl/supl.*
Copyright: (C) 2007 Tatu Mannisto <tatu@tajuma.com>
License: BSD-2-clause

Files:
 src/core/libs/supl/asn-supl/*
 src/core/libs/supl/asn-rrlp/*
Copyright: (C) 2004, 2006 Lev Walkin <vlm@lionet.info>
License: BSD-2-clause

Files: src/core/libs/INIReader.*
       src/core/libs/ini.*
Copyright: (C) 2009 Brush Technologies
License: BSD-3-clause

Files: src/algorithms/libs/volk_gnsssdr_module/volk_gnsssdr/*
Copyright: (C) 2014 Andres Cecilia <a.cecilia.luque@gmail.com>
License: GPL-3+

Files: src/algorithms/signal_source/adapters/rtl_tcp_signal_source.*
Copyright: (C) 2015 Anthony Arnold <anthony.arnold@uqconnect.edu.au>
License: GPL-3+

Files:
 src/algorithms/libs/volk_gnsssdr_module/volk_gnsssdr/kernels/volk_gnsssdr/volk_gnsssdr_s32f_sincos_32fc.h
 src/algorithms/libs/volk_gnsssdr_module/volk_gnsssdr/kernels/volk_gnsssdr/volk_gnsssdr_32f_sincos_32fc.h
Copyright: (C) 2007 Julien Pommier
License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files: debian/*
Copyright: (C) 2015 Carles Fernandez <carles.fernandez@cttc.es>
License: GPL-3+

Files: cmake/Modules/CMakeParseArgumentsCopy.cmake
Copyright:  © 2010 Alexander Neundorf <neundorf@kde.org>
License: BSD-2-clause

Files: src/algorithms/libs/cl.hpp
Copyright: (C) 2008-2013 The Khronos Group Inc.
License: Permissive
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.

Files: src/algorithms/libs/clFFT.h
       src/algorithms/libs/fft_execute.cc
       src/algorithms/libs/fft_base_kernels.h
       src/algorithms/libs/fft_internal.h
       src/algorithms/libs/fft_kernelstring.cc
       src/algorithms/libs/fft_setup.cc
Copyright: (C) 2008 Apple Inc.
License: Apple-Permissive
 This Apple software is supplied to you by Apple Inc. ("Apple")
 in consideration of your agreement to the following terms, and your use,
 installation, modification or redistribution of this Apple software
 constitutes acceptance of these terms.  If you do not agree with these
 terms, please do not use, install, modify or redistribute this Apple
 software.
 .
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non - exclusive
 license, under Apple's copyrights in this original Apple software ( the
 "Apple Software" ), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and / or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following text
 and disclaimers in all such redistributions of the Apple Software. Neither
 the name, trademarks, service marks or logos of Apple Inc. may be used to
 endorse or promote products derived from the Apple Software without specific
 prior written permission from Apple.  Except as expressly stated in this
 notice, no other rights or licenses, express or implied, are granted by
 Apple herein, including but not limited to any patent rights that may be
 infringed by your derivative works or by other works in which the Apple
 Software may be incorporated.
 .
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE MAKES NO
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 WARRANTIES OF NON - INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND OPERATION
 ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 .
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES ( INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION ) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION
 AND / OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED AND WHETHER
 UNDER THEORY OF CONTRACT, TORT ( INCLUDING NEGLIGENCE ), STRICT LIABILITY OR
 OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution. Neither the name of the author
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the University nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
